﻿using System;
using System.Collections.Generic;
using UI = Gtk.Builder.ObjectAttribute;
using MarkdownSharp;
using System.IO;
using System.Text.RegularExpressions;
using obj = System.Collections.Generic.Dictionary<string, dynamic>;
using kvobj = System.Collections.Generic.KeyValuePair<string, dynamic>;
using Cripts = System.Collections.Generic.List<Scripten>;
using fun = System.Func<dynamic[], dynamic>;

struct Mnum<T>
{
    private Dictionary<string, T> mnums;
    
    public Mnum(params T[] vars)
    {
        mnums = new Dictionary<string, T>();
        foreach(var vas in vars)
        {
            mnums[$"{vas}"] = vas;
        }
    }
}

class Interpreter
{
    public static fun FuncNews(string s)
    {
        return null;
    }
    public static obj TypeNews(string s)
    {
        return null;
    }
    public static obj Do(params dynamic[] arg)
    {
        return null;
    }
}

class Scripten
{
    
    public Scripten(string script_name)
    {
        objects.Add("Local", new obj());
        objects.Add("Class", new obj());
        objects.Add("Global", new obj());
        var codes = new List<string>();
        using (var file = new StreamReader(script_name))
        {
            var s = "";
            while ((s = file.ReadLine()) is not null)
                codes.Add(s);
        }
        //何かしらの操作を行う
        _ = Pass;
    }
    
    obj objects= new obj();
    obj classter = new obj();
    List<fun> function = new List<fun>();

    private fun MakeFunction(string[] code)
    {
        //関数作成
        //関数名をGlobalに追加
        /* function Fname
         *      ###
         *      $list 全ての引数
         *      $n 引数
         *      $@ 引数数
         *      ###
         *      for i in 0...$@
         *          if $i
         *              t = $i
         */

        _ = Pass;
        return null;
    }

    public bool IsNullOrEmpty<T>(ICollection<T> collection)
    {
        return collection == null || collection.Count == 0;
    }

    /// <summary>T []がnullまたは空であれば真</summary>
    public bool IsNullOrEmpty<T>(T[] array)
    {
        return (array == null || array.Length == 0);
    }

    private bool IsNullOrEmpty(dynamic inobject)
    {
        if (!inobject) { return true; }
        bool isobj = true;
        try
        {
            if (GetTypes(inobject) == typeof(List<dynamic>).Name) 
            {
                isobj=IsNullOrEmpty((List<dynamic>)inobject);
            }
            else if (GetTypes(inobject) == typeof(dynamic[]).Name)
            {
                isobj=IsNullOrEmpty((dynamic[])inobject);
            }
        }
        catch (Exception)
        {
            ;//メソッドがない場合何かしらのtrueの値が存在するので無視する
        }
        return isobj;
    }

    private void MakeObjects(string labels,string[] name,kvobj setter)
    {
        var lbl = objects[labels];
        foreach(var o in name)
        {
            lbl = lbl[o];
        }
        var obj_val = (obj)lbl;
        obj_val[setter.Key] = setter.Value;
    }

    private string GetTypes(dynamic any)
    {
        string objs = any.GetType().Name;
        if (objs == typeof(obj).Name)
        {
            return typeof(object).Name;
        }
        else
        {
            return objs;
        }
    }

    private dynamic GetObjects(string labels,string[] name)
    {
        try
        {
            var lbl = objects[labels];
            foreach(var o in name) 
            {
                lbl = lbl[o];
            }
            return lbl;
        }
        catch (Exception)
        {
            return null;
        }
    }

    private dynamic SetObjects(string labels, string[] name, kvobj setter)
    {
        try
        {
            var lbl = objects[labels];
            foreach (var o in name)
            {
                lbl = lbl[o];
            }
            lbl[setter.Key] = setter.Value;
        }
        catch (Exception)
        {
            return setter;
        }
        return setter.Value;
    }

    private dynamic Add(dynamic o1,dynamic o2)
    {
        try
        {
            return o1 + o2;
        }
        catch (Exception)
        {
            var od1 = (obj)o1;
            var od2 = (obj)o2;
            var ret = Interpreter.Do(od1["Objects"]["+"], od2);
            return ret;
        }
    }
    
    private dynamic Append(ref dynamic o1,dynamic o2)
    {
        try
        {
            o1 += o2;
        }
        catch (Exception)
        {
            var od1 = (obj)o1;
            var od2 = (obj)o2;
            var ret = Interpreter.Do(od1["Objects"]["+="], od2);
            return ret;
        }
        return o1;
    }

    private obj Initter(string s,obj cls)
    {
        var o = cls[s];
        return new obj(o);
    }

    private void TypeNewer(string name,string o)
    {
        obj ob = Interpreter.TypeNews(o);
        classter.Add(name, ob);
    }

    public object Pass{ get { return null; } }
}

class Emily
{

}

namespace MemoApp
{
    using Gtk;
    using System.IO;
    //Hyper Text Texture Notation Expansion
    public class HyperTextTextureNotation : Markdown
    {
        public HyperTextTextureNotation(string dir) { this.dir = dir; }
        private string Expansion(string text)
        {
            return text;
        }
        public string Changeing()
        {
            using (var w = new StreamReader(dir))
            {
                text = w.ReadToEnd();
            }
            var ret = httn.ChangeingText(text);
            return ret;
        }

        //この中で変換作業を行う。
        public string ChangeingText(string text)
        {
            text = new Regex(@"-\[ \]\(\)").Replace(text, $"<input type='checkbox' name='checkbox' value='{num}'>");
            text = new Regex(@"-\[x\]\(\)").Replace(text, $"<input type='checkbox' name='checkbox' value='{num}' checked='checked'>");
            //動画を入れる !>[試しの動画](https://www.youtube.com/watch?v=sample)
            //動画を入れる !>[試しの動画](./sample.mp4)
            text = new Regex(@"!>\[{now}\]\({url}\)").Replace(text, "<video src={url}poster={now} controls></video>");
            //動的埋め込み
            text = new Regex(@"@req {name}").Replace(text, "name.emily");
            text = Transform(text);
            return text;
        }

        public static string Changeing(string text)
        {
            var ret = httn.ChangeingText(text);
            return ret;
        }
        private readonly string dir;
        private string text = "";
        private int num = 0;
        private static HyperTextTextureNotation httn = new HyperTextTextureNotation(@"");
    }

    class Program
    {
        private Cripts script = new Cripts();
        static void Main(string[] args)
        {
            var outpt = HyperTextTextureNotation.Changeing("# Orgs\n - 1\n - 2\n\n -[ ]p\n -[x]q");

            Console.WriteLine(outpt);
            //Cross platform memo app 
            Init.Check(ref args);
			var window = new Window(WindowType.Toplevel);
			
            window.Title = "NotesApp";

			var fix = new Fixed();

			var button1 = new Button("button 1");
			fix.Put(button1, 150, 50);
			button1.SetSizeRequest(80, 28);


			var button2 = new Button("button 2");
			fix.Put(button2, 15, 15);
			button2.SetSizeRequest(80, 28);


			var button3 = new Button("button 3");
			fix.Put(button3, 100, 100);
			button3.SetSizeRequest(80, 28);

            window.SetSizeRequest(400, 250);
            window.Add(fix);
            window.ShowAll();
            while (true) ;
        }
	}
}
