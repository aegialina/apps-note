
class StartUp():
    __this = None

    def __init__(__self):
        pass
    
    @classmethod
    def Get(cls):
        if cls.__this is not None:
            return cls.__this
        cls.__this = StartUp()
        return cls.__this

    @classmethod
    def main(cls):
        cls.Get()
        print("Hello World")
        cls.__this.input = input("何か入力")
        return

if __name__ == "__main__":
    StartUp.main()
