import sys
import argparse
import re
import py_compile
import zipapp
import io

class File(dict):
    @classmethod
    def Load(cls,path):
        data = None
        with open(path,'r',encoding="utf-8") as fln:
            var = []
            for f in fln.read():
                if('__import__' in f.split('(')):
                    continue
                else:
                    var.append(f)
            data = cls.__Ld(var)
        try:
            ret = eval(data)
        except:
            ret = None
            print("pysonファイルが不適切なため実行できません")
        return ret

    @classmethod
    def __Ld(cls,var):
        ret = ""
        for val in var:
            ret += val
        return ret

    @classmethod
    def Using(cls,value,namespace=[]):
        obj = value
        for var in namespace:
            try:
                obj = obj[var]
            except expression as ex:
                return None
        return obj

    @classmethod
    def ConfigCompile(cls,obj):
        ret = py_compile(obj)
        pass

load = File.Load
using = File.Using
cfgcpl = File.ConfigCompile